﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        int X, Y, X1, Y1;
        bool IsClicked = false;
        bool pen, line = true, rectangle, ellipse;
        bool fill = false;
        int sizeofline = 1;
        Graphics gr;
        Bitmap bmp;
        Color color = Color.Black;
        public Form1()
        {
            InitializeComponent();
            bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            gr = Graphics.FromImage(bmp);
        }
        Point current, prev;
        
        void filling(int x,int y,Color clr)
        {
            if (x < 1) { return; }
            if(y < 1) { return; }
            if (x > pictureBox1.Width - 1 || y > pictureBox1.Height - 1) { return; }
            if (bmp.GetPixel(x, y) == clr)
            {
                gr.DrawLine(new Pen(color,1), new Point(x, y), new Point(x + 1, y));
               
                filling(x, y + 1, clr);
                filling(x - 1, y, clr);
                filling(x, y - 1, clr);
               
                filling(x + 1, y, clr);

            }
               
                
        }

     
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            IsClicked = true;
            if(fill)
            {

                Color clr = bmp.GetPixel(e.X, e.Y);
                if (bmp.GetPixel(e.X, e.Y) != color)
                {
                    filling(e.X, e.Y, clr);
                }
                
            }
            if (pen)
            {
                prev = e.Location;
                current = prev;
            }
            X = e.X;
            Y = e.Y;
            
            
        }
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            IsClicked = false;

            Pen p = new Pen(color, sizeofline);
            if (line)
            {
               
                gr.DrawLine(p, X, Y, X1, Y1);
            }

            if (rectangle)
            {
                if (rectangle)
                {
                    if (X1 < X && Y1 < Y)
                    {
                        gr.DrawRectangle(p, X1, Y1, X - X1, Y - Y1);
                        
                        return;
                    }

                    if (X1 < X)
                    {
                        gr.DrawRectangle(p, X1, Y, X - X1, Y1 - Y);
                        pictureBox1.Image = bmp;
                        return;
                    }

                    if (Y1 < Y)
                    {
                        gr.DrawRectangle(p, X, Y1, X1 - X, Y - Y1);
                        pictureBox1.Image = bmp;
                        return;
                    }


                    
                }
                    gr.DrawRectangle(p, X, Y, X1 - X, Y1 - Y);
                
            }

            if (ellipse)
            {
                gr.DrawEllipse(p, X, Y, X1-X, Y1-Y);
            }

            pictureBox1.Image = bmp;
        
            
        }

        
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsClicked)
            {

                X1 = e.X;
                Y1 = e.Y;
                pictureBox1.Invalidate();

                if(pen)
                {
                    prev = current;
                    current = e.Location;
                }
                
            }
            
          
        }
       

     

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            Pen p = new Pen(color, sizeofline);
            if (line)
            {

                e.Graphics.DrawLine(p, X, Y, X1, Y1);
            }

         
            if (rectangle)
            {
                if (X1 < X && Y1 < Y)
                {
                    e.Graphics.DrawRectangle(p, X1, Y1, X - X1, Y - Y1);
                    return;
                }

                if (X1 < X)
                {
                    e.Graphics.DrawRectangle(p, X1, Y, X - X1, Y1 - Y);
                    return;
                }

                if (Y1 < Y)
                {
                    e.Graphics.DrawRectangle(p, X, Y1, X1 - X, Y - Y1);
                    return;
                }
              

              e.Graphics.DrawRectangle(p, X, Y, X1 - X, Y1 - Y);
            }

            
            if(ellipse)
            {
         
                e.Graphics.DrawEllipse(p, X, Y, X1-X, Y1-Y);
            }

            if(pen)
            {
                Pen q = new Pen(color,1);
                gr.DrawLine(q,prev,current);               
                pictureBox1.Image = bmp;
            } 
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ellipse = false;
            rectangle = false;
            pen = false;
            line = true;
            fill = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ellipse = false;
            rectangle = true;
            pen = false;
            line = false;
            fill = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ellipse = true;
            rectangle = false;
            pen = false;
            line = false;
            fill = false;
        }

        
            


        

        private void button4_Click(object sender, EventArgs e)
        {
            ellipse = false;
            rectangle = false;
            pen = true;
            line = false;
            fill = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(pen)
            {
                pen = false;
                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    color = colorDialog1.Color;
                }
                pen = true;
                return;
            }
            if(colorDialog1.ShowDialog() == DialogResult.OK)
            {
                color = colorDialog1.Color;
            }


        }
        int oldsize;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string a = textBox1.Text;
            for (int i = 0; i <= a.Length - 1;i++ )
            {
                if (a[i] < 48 || a[i] > 57) { textBox1.Text = oldsize.ToString(); }
            }
                sizeofline = int.Parse(textBox1.Text);
            oldsize = sizeofline;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ellipse = false;
            rectangle = false;
            pen = false;
            line = false;
            fill = true;
        }


   
    }
}
