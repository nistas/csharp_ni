﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication2.Objects;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            WindowSize Window = new WindowSize();
            Console.SetWindowSize(Window.Width, Window.Height);
            Snake worm = new Snake();
            Snake worm1 = new Snake();
            ConsoleKeyInfo key = new ConsoleKeyInfo();
            List<Food> food = new List<Food>();
            food.Add(new Food());
            food[food.Count - 1].Create();
            worm.Snakes(15, 15);
            worm1.Snakes1(10, 30);
            while (1 > 0)
            {
                int sx = 0;
                int sy = 0;
                int dx = 0;
                int dy = 0;
                key = Console.ReadKey();
                if (key.Key == ConsoleKey.UpArrow)
                {
                    dx = 0; dy = -1;
                }

                if (key.Key == ConsoleKey.DownArrow)
                {
                    dx = 0; dy = 1;
                }

                if (key.Key == ConsoleKey.LeftArrow)
                {
                    dx = -1; dy = 0;
                }

                if (key.Key == ConsoleKey.RightArrow)
                {
                    dx = 1; dy = 0;
                }

                if (key.Key == ConsoleKey.W)
                {
                    sx = 0; sy = -1;
                }

                if (key.Key == ConsoleKey.S)
                {
                    sx = 0; sy = 1;
                }

                if (key.Key == ConsoleKey.A)
                {
                    sx = -1; sy = 0;
                }

                if (key.Key == ConsoleKey.D)
                {
                    sx = 1; sy = 0;
                }

                if (key.Key == ConsoleKey.Escape)
                {
                    return;
                }

                if (key.Key == ConsoleKey.O)
                {
                    Console.SetCursorPosition(1, 1);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(dx + dy);
                    Console.ForegroundColor = ConsoleColor.Black;
                }

                if (dx != 0 || dy != 0) 
                {
                    if (worm.Move(dx, dy, food[food.Count - 1].Location) == true)
                    {

                        food.Add(new Food());
                        food[food.Count - 1].Create();
                    }
                }

                if (sx != 0 || sy != 0)
                {
                    if (worm1.Move1(sx, sy, food[food.Count - 1].Location) == true)
                    {

                        food.Add(new Food());
                        food[food.Count - 1].Create();
                    }
                    Console.SetCursorPosition(0, 0);
                }

            }

        }
    }
}