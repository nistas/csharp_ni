﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2.Objects
{
    public class wall
    {
        public bool[,] FreeCell = new bool[400, 400];

        char sign = '#';
        Point[] Wall = new Point[256];

        public wall(int width, int height)
        {
            for (int i = 0; i < 400; i++)
                for (int j = 0; j < 400; j++)
                {
                    FreeCell[i, j] = true;
                }

            for (int i = 0; i < width; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write(sign);
                FreeCell[i, 0] = false;
                Console.SetCursorPosition(i, width - 1);
                Console.Write(sign);
                FreeCell[i, width - 1] = false;
            }

            for (int i = 0; i < height; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write(sign);
                FreeCell[0, i] = false;
                Console.SetCursorPosition(height - 1, i);
                FreeCell[height - 1, i] = false;
                Console.Write(sign);
            }

        }
    }
}
