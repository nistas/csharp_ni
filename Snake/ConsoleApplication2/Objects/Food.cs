﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2.Objects
{
    class Food
    {
        Random rand = new Random();
        WindowSize Window = new WindowSize();
        char sign = '*';
        public Point Location = new Point();

        public void Create()
        {
           
            Location.x = rand.Next(1,Window.Width-1);
            Location.y = rand.Next(1, Window.Height-1);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.SetCursorPosition(Location.x, Location.y);
            Console.Write(sign);
            Console.SetCursorPosition(0, 0);
            Console.ForegroundColor = ConsoleColor.Black;
        }

    }
}
