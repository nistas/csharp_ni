﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication6
{
    public partial class Form1 : Form
    {
        MyButton[,] cell = new MyButton[10, 10];
        MyButton[,] cellC = new MyButton[10, 10];
        int[,] counter = new int[4, 4];
        Field user = new Field();
        Field comp = new Field();
        public Form1()
        {
           
            InitializeComponent();
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {

                    cell[i, j] = new MyButton(i, j);
                    cell[i, j].Location = new Point(i * 20 + 70, j * 20 + 70);
                    cell[i, j].Size = new Size(20, 20);
                    cell[i, j].BackColor = Color.Brown;
                    cell[i, j].Click += new System.EventHandler(Cell_Click);
                    this.Controls.Add(cell[i, j]);
                }


            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    cellC[i, j] = new MyButton(i, j);
                    cellC[i, j].Location = new Point(i * 20 + 500, j * 20 + 70);
                    cellC[i, j].Size = new Size(20, 20);
                    cellC[i, j].BackColor = Color.Brown;                
                    cellC[i, j].Click += new System.EventHandler(Cellc_Click);
                    this.Controls.Add(cellC[i, j]);
                }

          comp.Create();


          



        }
        bool full = false;
        int moveuser = 20;
        int movecomp = 20;
        private void Cellc_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            bool IsLast = true;
            cellC[b.x, b.y].Enabled = false;
            if (comp.cell[b.x, b.y] == 0)
            {
                cellC[b.x, b.y].BackColor = Color.White;
            }
            if (comp.cell[b.x, b.y] == 1)
            {
                movecomp--;
                for (int i = b.x - 1; i <= b.x + 1;i++ )
                {
                    if (i < 0) { i++; }
                    if (i > 9) { break; }
                    for( int j = b.y; j <= b.y ; j++)
                    {
                        if (j < 0) { j++; }
                        if (j > 9) { break; }
                        if (comp.cell[i, j] == 1) { IsLast = false;}

                    }
                }
                if (IsLast) { cellC[b.x, b.y].BackColor = Color.Red;  }
                if (IsLast == false) { cellC[b.x, b.y].BackColor = Color.Green; comp.cell[b.x, b.y] = 5; }
                
                    
            }
            if (moveuser < 1) { MessageBox.Show("Ты проиграл!"); }
            if (movecomp < 1) { MessageBox.Show("Ты Выиграл!!!"); }


            Random rnd = new Random();
            int x, y;
            x = rnd.Next(0, 9);
            y = rnd.Next(0, 9);
            if (user.cell[x, y] == 1) { cell[x, y].BackColor = Color.Red; moveuser--; }
            if (user.cell[x, y] == 0) { cell[x, y].BackColor = Color.White; }
            cell[x,y].Enabled = false;
        }

        int qwe = 0;
        private void Cell_Click(object sender, EventArgs e)
        {
            int CellsCount;

            MyButton b = (sender as MyButton);
           
            CellsCount = Convert.ToInt16(comboBox1.SelectedItem);

            if (counter[0, 1] > 3 && CellsCount == 1) { return; }
            if (counter[1, 1] > 2 && CellsCount == 2) { return; }
            if (counter[2, 1] > 1 && CellsCount == 3) { return; }
            if (counter[3, 1] > 0 && CellsCount == 4) { return; }

            if (qwe == 10)
            {

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        cellC[i,j].Enabled = true;
                    }
                }
            }


            if (radioButton1.Checked)
            {
                for (int i = b.x; i < CellsCount + b.x; i++)
                {
                    if (i == 10) { break; }
                    user.cell[i, b.y] = 1;
                    cell[i, b.y].BackColor = Color.Blue;
                    cell[i, b.y].Enabled = false;

                }
                qwe++;
            }

            if (radioButton2.Checked)
            {
                for (int j = b.y; j < CellsCount + b.y; j++)
                {
                    if (j == 10) { break; }
                    user.cell[b.x, j] = 1;
                    cell[b.x, j].BackColor = Color.Blue;
                    cell[b.x, j].Enabled = false;
                }
                qwe++;
            }

            if (CellsCount == 1) { counter[0, 1]++; }
            if (CellsCount == 2) { counter[1, 1]++; }
            if (CellsCount == 3) { counter[2, 1]++; }
            if (CellsCount == 4) { counter[3, 1]++; }
            


        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (comp.cell[i, j] == 1) { cellC[i, j].BackColor = Color.Black; }
                    }
                }
            }
        }
    }
}

            

  
  
