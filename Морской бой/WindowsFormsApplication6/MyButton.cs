﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication6
{
    class MyButton : Button
    {
        public int x;
        public int y;

        public MyButton(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
    }
}
