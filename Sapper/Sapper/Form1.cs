﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sapper
{
    public partial class Form1 : Form
    {
       static int x, y,b,i,j,c,z;
        public Form1()
        {
            InitializeComponent();
            

        }
        int[,] bomb;
        private void zeropoint(int x2, int y2)
        {
            int o, p;

            for (o = x2 - 1; o <= x2 + 1; o++)
            {
                if (o < 0) { o++; }
                if (o < 0) { o++; }
                if (o == x) { break; }
                for (p = y2 - 1; p <= y2 + 1; p++)
                {
                    if (p < 0) { p++; }
                    if (p == y) { break; }
                    if (p < 0) { p++; }
                    if (Movee(o, p) == 0 && button[o, p].Enabled == true) { button[o, p].Enabled = false; button[o, p].BackColor = Color.White; if (o != 0 && y!=0) { zeropoint(o, p); } }
                    else { if (Movee(o, p) != 0 && button[o, p].Enabled == true) { button[o, p].Text = Movee(o, p).ToString(); button[o, p].BackColor = Color.White; button[o, p].Enabled = false; } }
                }
            }




        }

        private void buttonclick(object sender, EventArgs e)
        {
            MyButton a = sender as MyButton;
            if (bomb[a.x, a.y] == 1) { a.Text = "*"; MessageBox.Show("Ты проиграл!"); timer1.Enabled = false; label1.Visible = false; this.Controls.Clear(); InitializeComponent(); }
            if (bomb[a.x, a.y] == 0)
            {
                a.Text = Movee(a.x, a.y).ToString();
                if (Movee(a.x, a.y) == 0) { a.Text = ""; zeropoint(a.x, a.y); }

                a.Enabled = false;
                a.BackColor = Color.White;
            }
        }
        int Movee(int x1, int y1)
        {
            int k = 0;
            
           
           for (i = x1 - 1; i <= x1 + 1; i++)
            {
                if (x1 < 0) { x1 = x1 + 2; }
                if (x1 == 0) { x1++; }
                if (i < 0) { i++; }
                if (i < 0) { i++; }
                if (i == x) { break; }
                for (j = y1-1; j <= y1 + 1; j++)
                {
                   
                    if (j < 0 ) { j++; }
                    if (j == y) { break; }
                    if (bomb[i, j] == 1) { k++; }

                }
            } 
            return k;
        }
        int time;
        MyButton[,] button;
        private void button1_Click(object sender, EventArgs e)
        {
            time = 0;
            label1.Visible = true;
            listBox1.Visible = false;
            button1.Visible = false;
            timer1.Enabled = true;
           if (listBox1.SelectedIndex == 2) { x = 9; y = 9; b = 10;  }
           if (listBox1.SelectedIndex == 1) { x = 16; y = 16; b = 40; }
           if (listBox1.SelectedIndex == 0) { x = 30; y = 16; b = 99;  }
           button = new MyButton[x, y];
           bomb = new int[x, y];
            for (i = 0; i < x; i++)
           {
               for (j = 0; j < y; j++)
               {
                   bomb[i,j] = 0;
               }
            }
            Random rnd= new Random();
            for (i = 0; i < b; i++)
            {
                c = rnd.Next(0, x);
                z = rnd.Next(0, y);
                if (bomb[c, z] == 1) { i--; };
                bomb[c, z] = 1;
            }
           for (i = 0; i < x; i++)
           {
               for (j = 0; j < y; j++)
               {
                   button[i,j] = new MyButton(i,j);
                   Point p = new Point(i * 30, j * 30+50);
                   Size s = new Size(30, 30);
                   button[i, j].Location = p;
                   button[i, j].Size = s;
                   button[i,j].MouseDown += new MouseEventHandler(MouseDown1);
                   button[i, j].Click += new EventHandler(buttonclick);


                   this.Controls.Add(button[i, j]);
               }
           }

        }
        
        private void MouseDown1(object sender, MouseEventArgs e)
        {
            Button a = sender as Button;
            if (e.Button == MouseButtons.Right && a.BackColor == Color.Red) { a.BackColor = Color.Wheat; a.Text = "?"; return; }
            if (e.Button == MouseButtons.Right) { a.BackColor = Color.Red; a.Text = ""; }
           
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            
            time++;
            label1.Text ="Время:" + time.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        
    }
}
