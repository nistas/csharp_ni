﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    class BigNumber
    {
        
            String b;

            public BigNumber(String _b)
            {

                b = _b;
            }

            public override string ToString()
            {
                return b;
            }


            public static BigNumber operator +(BigNumber a, BigNumber g)
            {
                int[] q = new int[10000];
                int[] w = new int[10000];
                int i;
                for (i = 0; i < a.b.Length; i++)
                {
                    q[i + 1] = a.b[i] - 48;
                }
                for (i = 0; i < g.b.Length; i++)
                {
                    w[i + 1] = g.b[i] - 48;
                }

                if (a.b.Length > g.b.Length)
                {
                    for (i = 0; i < g.b.Length; i++)
                    {
                        q[a.b.Length - i] += w[g.b.Length - i];
                    }
                    for (i = 0; i < a.b.Length; i++)
                    {
                        if (q[a.b.Length - i] >= 10) { q[a.b.Length - i] -= 10; q[a.b.Length - i - 1] += 1; }

                    }
                    int qw = a.b.Length,j=1;
                    if (q[0] == 1) { j = 0; }
                    
                    a.b = "";

                    for (i = j; i <= qw; i++)
                    {
                        a.b += q[i];
                    }
                }
                if (a.b.Length < g.b.Length)
                {
                    for (i = 0; i < a.b.Length; i++)
                    {
                        w[g.b.Length - i] += q[a.b.Length - i];
                    }
                    for (i = 0; i < g.b.Length; i++)
                    {
                        if (w[g.b.Length - i] >= 10) { w[g.b.Length - i] -= 10; w[g.b.Length - i - 1] += 1; }

                    }
                    int qw = g.b.Length,j = 1;
                    if (w[0] == 1) { j = 0; }
                    a.b = "";

                    for (i = j; i <= qw; i++)
                    {
                        a.b += w[i];
                    }
                }


                return a;

            }
            public static BigNumber operator -(BigNumber a, BigNumber g)
            {
                int[] q = new int[10000];
                int[] w = new int[10000];
                int i;
                for (i = 0; i < a.b.Length; i++)
                {
                    q[i + 1] = a.b[i] - 48;
                }
                for (i = 0; i < g.b.Length; i++)
                {
                    w[i + 1] = g.b[i] - 48;
                }

                if (a.b.Length >= g.b.Length)
                {
                    for (i = 0; i < g.b.Length; i++)
                    {
                        q[a.b.Length - i] -= w[g.b.Length - i];
                    }
                    for (i = 0; i < a.b.Length; i++)
                    {
                        if (q[a.b.Length - i] < 0) { q[a.b.Length - i] += 10; q[a.b.Length - i - 1] -= 1; }

                    }

                    int qw = a.b.Length;
                    a.b = "";
                    int k = 0;
                    for (i = 0; i <= qw; i++)
                    {
                        if (q[i] != 0) { k++; }
                        if (k != 0)
                        {
                            a.b += q[i];
                        }
                    }

                }

                if (a.b.Length < g.b.Length)
                {
                    for (i = 0; i < a.b.Length; i++)
                    {
                        w[g.b.Length - i] -= q[a.b.Length - i];
                    }
                    for (i = 0; i < g.b.Length; i++)
                    {
                        if (w[g.b.Length - i] < 0) { w[g.b.Length - i] += 10; w[g.b.Length - i - 1] -= 1; }

                    }
                    int qw = g.b.Length;
                    a.b = "-";

                    int k = 0;
                    for (i = 0; i <= qw; i++)
                    {
                        if (w[i] != 0) { k++; }
                        if (k != 0)
                        {
                            a.b += w[i];
                        }
                    }
                }

                return a;
            }
           
        }
    }

