﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            int k = 1;
            InitializeComponent();
            int i,j;
            for (i = 3; i >= 1; i--)
            {
                for (j = 1; j <= 3; j++)
                {
                    MyButton[,] btns = new MyButton[4, 5];
                    btns[i,j] = new MyButton(i, j);
                    btns[i,j].Text += k.ToString();
                    Point p = new Point(j * 50-50, i * 50+100);
                    Size s = new Size(50,50);
                    btns[i, j].Size = s;
                    btns[i, j].Location = p;
                    
                    btns[i, j].Click += new System.EventHandler(button_Click);
                    this.Controls.Add(btns[i, j]);
                    k++;
                }
            }
            Button zero = new Button();
            Point powe = new Point(0, 300);
            Size size = new Size(100,50);
            zero.Location = powe;
            zero.Size = size;
            zero.Text = "0";
            zero.Click += new System.EventHandler(button_Click);
            this.Controls.Add(zero);

            Button multi = new Button();
            Point pow3 = new Point(150, 200);
            Size size4 = new Size(50, 50);
            multi.Location = pow3;
            multi.Size = size4;
            multi.Text = "*";
            multi.Click += new System.EventHandler(multi_Click);
            this.Controls.Add(multi);

            Button divided = new Button();
            Point pow4 = new Point(150, 150);
            Size size5 = new Size(50, 50);
            divided.Location = pow4;
            divided.Size = size5;
            divided.Text = "/";
            divided.Click += new System.EventHandler(divided_Click);
            this.Controls.Add(divided); 

            Button plus = new Button();
            Point pw = new Point(150, 300);
            Size sz = new Size(50, 50);
            plus.Location = pw;
            plus.Size = sz;
            plus.Text = "+";
            plus.Click += new System.EventHandler(plus_Click);
            this.Controls.Add(plus);

            Button equal = new Button();
            Point pwr = new Point(200, 250);
            Size sez = new Size(50, 100);
            equal.Location = pwr;
            equal.Size = sez;
            equal.Text = "=";
            equal.Click += new System.EventHandler(equal_Click);
            this.Controls.Add(equal);

            Button clear = new Button();
            Point po = new Point(100,100);
            Size si = new Size(50, 50);
            clear.Location = po;
            clear.Size = si;
            clear.Text = "C";
            clear.Click += new System.EventHandler(C_Click);
            this.Controls.Add(clear);

            Button clearE = new Button();
            Point poq = new Point(50, 100);
            Size sic = new Size(50, 50);
            clearE.Location = poq;
            clearE.Size = sic;
            clearE.Text = "CE";
            clearE.Click += new System.EventHandler(CE_Click);
            this.Controls.Add(clearE);

            Button minus = new Button();
            Point poi = new Point(150, 250);
            Size siz = new Size(50, 50);
            minus.Location = poi;
            minus.Size = siz;
            minus.Text = "-";
            minus.Click += new System.EventHandler(minus_Click);
            this.Controls.Add(minus);

           Button point = new Button();
            Point poin = new Point(100, 300);
            Size size1 = new Size(50, 50);
            point.Location = poin;
            point.Size = size1;
            point.Text = ",";
            point.Click += new System.EventHandler(button_point);
            this.Controls.Add(point);

            Button del = new Button();
            Point a321 = new Point(0, 100);
            Size size2 = new Size(50, 50);
            del.Location = a321;
            del.Size = size2;
            del.Text = "←";
            del.Click += new System.EventHandler(del1);
            this.Controls.Add(del);

            Button plusminus = new Button();
            Point a228 = new Point(150, 100);
            Size size123 = new Size(50, 50);
            plusminus.Location = a228;
            plusminus.Size = size123;
            plusminus.Text = "±";
            plusminus.Click += new System.EventHandler(plusminus1);
            this.Controls.Add(plusminus);

            Button sqrt = new Button();
            Point a229 = new Point(200, 100);
            Size size1234 = new Size(50, 50);
            sqrt.Location = a229;
            sqrt.Size = size1234;
            sqrt.Text = "√";
            sqrt.Click += new System.EventHandler(sqrt1);
            this.Controls.Add(sqrt);

            Button divbyone = new Button();
            Point rewq = new Point(200, 200);
            Size size12345 = new Size(50, 50);
            divbyone.Location = rewq;
            divbyone.Size = size12345;
            divbyone.Text = "1/x";
            divbyone.Click += new System.EventHandler(divbyone1);
            this.Controls.Add(divbyone);

            Button percent = new Button();
            Point prcnt = new Point(200, 150);
            Size s12 = new Size(50, 50);
            percent.Location = prcnt;
            percent.Size = s12;
            percent.Text = "%";
            percent.Click += new System.EventHandler(percent1);
            this.Controls.Add(percent);

            Button MS = new Button();
            Point poqwe = new Point(100, 50);
            Size siqwe = new Size(50, 50);
            MS.Location = poqwe;
            MS.Size = siqwe;
            MS.Text = "MS";
            MS.Click += new System.EventHandler(MS1);
            this.Controls.Add(MS);

            Button MR = new Button();
            Point ppc = new Point(50, 50);
            Size ewq = new Size(50, 50);
            MR.Location = ppc;
            MR.Size = ewq;
            MR.Text = "MR";
            MR.Click += new System.EventHandler(MR1);
            this.Controls.Add(MR);

            Button MC = new Button();
            Point ppcc = new Point(0, 50);
            Size ewqc = new Size(50, 50);
            MC.Location = ppcc;
            MC.Size = ewqc;
            MC.Text = "MC";
            MC.Click += new System.EventHandler(MC1);
            this.Controls.Add(MC);

            Button Mplus = new Button();
            Point psd = new Point(150, 50);
            Size eswc = new Size(50, 50);
            Mplus.Location = psd;
            Mplus.Size = eswc;
            Mplus.Text = "M+";
            Mplus.Click += new System.EventHandler(Mplus1);
            this.Controls.Add(Mplus);

            Button Mminus = new Button();
            Point psdd = new Point(200, 50);
            Size eswcd = new Size(50, 50);
            Mminus.Location = psdd;
            Mminus.Size = eswcd;
            Mminus.Text = "M-";
            Mminus.Click += new System.EventHandler(Mminus1);
            this.Controls.Add(Mminus);



            


        }

 
        double memory = 0;

        private void Mminus1(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            double w = double.Parse(textBox1.Text);
            memory -= w;
        }

        private void Mplus1(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            double w = double.Parse(textBox1.Text);
            memory += w ;
        }

        private void MC1(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            memory = 0;
            label2.Text = "";
        }

        private void MR1(object sender, EventArgs e)
        {
            textBox1.Text = memory.ToString();
        }

        private void MS1(object sender, EventArgs e)
        {

            if (textBox1.Text == "") { return; }
            if (textBox1.Text[0] < 58 && 47 < textBox1.Text[0] || textBox1.Text[0] == 45)
            {

                memory = Double.Parse(textBox1.Text);
                label2.Text = "M";

            }

        }

        private void percent1(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            if (textBox1.Text[0] < 58 && 47 < textBox1.Text[0] || textBox1.Text[0] == 45)
            {

                double q = double.Parse(textBox1.Text);
                double percent = (f/100) * q;
                textBox1.Text = percent.ToString();
                
            }

        }

        private void divbyone1(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            if (textBox1.Text[0] < 58 && 47 < textBox1.Text[0] || textBox1.Text[0] == 45)
            {

                double q = double.Parse(textBox1.Text);
                q = 1 / q;
                textBox1.Text = q.ToString();
            }

        }

        private void sqrt1(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            if (textBox1.Text[0] < 58 && 47 < textBox1.Text[0] || textBox1.Text[0] == 45)
            {

                double q = double.Parse(textBox1.Text);
                textBox1.Text = Math.Sqrt(q).ToString();
            }

        }

        private void plusminus1(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            if (textBox1.Text[0] < 58 && 47 < textBox1.Text[0] || textBox1.Text[0] == 45)
            {
                double q = 0;
                q = double.Parse(textBox1.Text);
                double w = 0;
                if (q > 0) { w = q - q * 2; }
                if (q < 0) { w = Math.Abs(q); }
                textBox1.Text = w.ToString();
            }

        }
        bool multienter, diventer, plusenter, minusenter;
        private void del1(object sender, EventArgs e)
        {
            int i,len = 0;
            string txt="";
            if (textBox1.Text == "") { return; }
            len = textBox1.Text.Length;
            if (textBox1.Text[0] < 58 && 47 < textBox1.Text[0])
            {
                for (i = 0; i <= len - 2; i++)
                {
                    txt += textBox1.Text[i];
                }
                textBox1.Text = txt;

            }
        }

        private void button_point(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            Button b = sender as Button;
            textBox1.Text = textBox1.Text + ",";
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (m || p || d || mu)
            {
                textBox1.Text = "";
                m = false;
                p = false;
                d = false;
                mu = false;
            }
            if (textBox1.Text == "0" && b.Text != "0") { textBox1.Text = b.Text; return; };
            if (textBox1.Text == "бесконечность") { textBox1.Text = ""; }
            if (textBox1.Text == "0" && b.Text == "0") { return; };
            textBox1.Text += b.Text;
        }
        double f=0;
     
         public void multi_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }
            if (m)
            {
                minusenter = false;
                m = false;
                textBox1.Text = f.ToString();
            }
             if(p)
             {
                 plusenter = false;
                 p = false;
                 textBox1.Text = f.ToString();
             }

             if (d)
             {
                 diventer = false;
                 d = false;
                 textBox1.Text = f.ToString();
             }

            if (mu) { return; }
            
            if (multienter || diventer || plusenter || minusenter)
            {
                equal(); ;
            }
            f = double.Parse(textBox1.Text);
            textBox1.Text += "*";
            multienter = true;
            mu = true;
        }

         public void divided_Click(object sender, EventArgs e)
         {
             if (textBox1.Text == "") { return; }
             if (m)
             {
                 minusenter = false;
                 m = false;
                 textBox1.Text = f.ToString();
             }
             if (p)
             {
                 plusenter = false;
                 p = false;
                 textBox1.Text = f.ToString();
             }

             if (mu)
             {
                 multienter = false;
                 mu = false;
                 textBox1.Text = f.ToString();
             }

             if (d) { return; }
             if (multienter || diventer || plusenter || minusenter)
             {
                 equal(); ;
             }
             f = double.Parse(textBox1.Text);
             textBox1.Text += "/";
             diventer = true;
             d = true;
         }

       public void plus_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { return; }

            if (m)
            {
                minusenter = false;
                m = false;
                textBox1.Text = f.ToString();
            }
            if (mu)
            {
                multienter = false;
                mu = false;
                textBox1.Text = f.ToString();
            }

            if (d)
            {
                diventer = false;
                d = false;
                textBox1.Text = f.ToString();
            }

            if (p) { return; }
            if (multienter || diventer || plusenter || minusenter)
            {
                equal(); ;
            }

            if (textBox1.Text.Length > 14) { a = new BigNumber(textBox1.Text); bignum = true; }
            else
            {
                f = double.Parse(textBox1.Text);
            }
            textBox1.Text += "+";
           plusenter = true;
           p = true;
        }
       bool bignum = false;
       public void minus_Click(object sender, EventArgs e)
       {
           if (textBox1.Text == "") { return; }
           if (p)
           {
               plusenter = false;
               p = false;
               textBox1.Text = f.ToString();
           }
           if (mu)
           {
               multienter = false;
               mu = false;
               textBox1.Text = f.ToString();
           }

           if (d)
           {
               diventer = false;
               d = false;
               textBox1.Text = f.ToString();
           }


           if (m) { return; }
           if (multienter || diventer || plusenter || minusenter)
           {
               equal(); ;
           }
          if (textBox1.Text.Length > 14) { a = new BigNumber(textBox1.Text); bignum = true; }
           else
           {
               f = double.Parse(textBox1.Text);
           }
           textBox1.Text += "-";
           minusenter = true;
           m = true;
       }
       BigNumber a,b;
      void equal()
       {

          


           if (minusenter)
           {
               doubleequal = double.Parse(textBox1.Text);
               f = f - double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               minusenter = false;
               return;
           }

           if (plusenter)
           {
               doubleequal = double.Parse(textBox1.Text);
               f = f + double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               plusenter = false;
               return;
           }

           if (multienter)
           {
               doubleequal = double.Parse(textBox1.Text);
               f = f * double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               multienter = false;
               return;
           }

           if (diventer)
           {
               doubleequal = double.Parse(textBox1.Text);
               f = f / double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               diventer = false;
               return;
           } 
       }

      
       bool min = false, plu = false, dev = false, mul = false,minbn = false,plusbn = false;
       bool m = false, p = false, d = false , mu = false;
       double doubleequal = 0, t = 0, res;
       public void equal_Click(object sender, EventArgs e)
       {
           StartPos:
           if (plusenter && bignum)
           {
               minbn = false;
               b = new BigNumber(textBox1.Text);
               BigNumber c = a + b;
               textBox1.Text = c.ToString();
               bignum = false;
               plusenter = false;
               plusbn = true;
               return;
           }

           if (minusenter && bignum)
           {
               plusbn = false;
               b = new BigNumber(textBox1.Text);
               BigNumber c = a - b;
               textBox1.Text = c.ToString();
               bignum = false;
               minusenter = false;
               minbn = true;
               return;
           }


           if (minusenter)
           {
               if (textBox1.Text.Length > 14) { a = new BigNumber(f.ToString()); bignum = true; goto StartPos; }
               min = false;
               plu = false;
               dev = false;
               mul = false;
               doubleequal = double.Parse(textBox1.Text);
               f = f - double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               minusenter = false;
               min = true;
               return;
           }

           if (plusenter)
           {
               if (textBox1.Text.Length > 14) { a = new BigNumber(f.ToString()); bignum = true; goto StartPos; }
               min = false;
               plu = false;
               dev = false;
               mul = false;
               doubleequal = double.Parse(textBox1.Text);
               f = f + double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               plusenter = false;
               plu = true;
               return;
           }

           if (multienter)
           {
               minbn = false;
               plusbn = false;
               min = false;
               plu = false;
               dev = false;
               mul = false;
               doubleequal = double.Parse(textBox1.Text);
               f = f * double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               multienter = false;
               mul = true;
               return;
           }

           if (diventer)
           {
               minbn = false;
               plusbn = false;
               min = false;
               plu = false;
               dev = false;
               mul = false;
               doubleequal = double.Parse(textBox1.Text);
               f = f / double.Parse(textBox1.Text);
               textBox1.Text = f.ToString();
               diventer = false;
               dev = true;
               return;
           }
           if (plusbn)
           {
               a = new BigNumber(textBox1.Text);
               BigNumber c = a + b;
               textBox1.Text = c.ToString();
           }

           if (minbn)
           {
               a = new BigNumber(textBox1.Text);
               BigNumber c = a - b;
               textBox1.Text = c.ToString();
           }
           
           if (min)
           {
               t = double.Parse(textBox1.Text);
               res = t - doubleequal;
               textBox1.Text = res.ToString();
           }

           if (plu)
           {
               t = double.Parse(textBox1.Text);
               res = t + doubleequal;
               textBox1.Text = res.ToString();
           }
           if (mul)
           {
               t = double.Parse(textBox1.Text);
               res = t * doubleequal;
               textBox1.Text = res.ToString();
           }


           if (dev)
           {
               t = double.Parse(textBox1.Text);
               res = t / doubleequal;
               textBox1.Text = res.ToString();
           }




        







          
         
       }

        public void C_Click(object sender, EventArgs e)
        {
            f = 0;
            doubleequal = 0;
            m = false;
            p = false;
            mu = false;
            d = false;
            minbn = false;
            plusbn = false;
            multienter = false;
            diventer = false;
            minusenter = false;
            plusenter = false;
            textBox1.Text = "";
        }

        public void CE_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        



        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        

      
    }
}
